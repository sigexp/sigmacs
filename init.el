;; Copyright © 2021-2024 Yevhenii Kolesnikov
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; --------------------------------
;; Package system
;; --------------------------------

;; Add package system
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))
(when (< emacs-major-version 24)
  (add-to-list 'package-archives
               '("gnu" . "http://elpa.gnu.org/packages/")))
(package-initialize)

(let ((default-directory (expand-file-name "lisp" user-emacs-directory)))
  (normal-top-level-add-subdirs-to-load-path))

;; Install use-package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; --------------------------------
;; Basic setup
;; --------------------------------

;; Set some defaults
(setq-default
 indent-tabs-mode          nil          ;Use spaces instead of tabs
 tab-width                 4            ;Tab width
 scroll-step               1            ;Scroll step
 fill-column               80           ;Page width
 select-enable-clipboard   t            ;Merge system and emacs clipboard
 x-stretch-cursor          t            ;Stretch cursor
 sentence-end-double-space nil          ;Sane sentence end
 delete-by-moving-to-trash t            ;Deleted files go to rubbish bin
 visible-bell              t            ;Flash instead of beeping
 use-package-always-ensure t            ;No reason not to
 ;; C style
 c-default-style           "k&r"        ;Style
 c-basic-indent            3            ;As in Mesa
 c-basic-offset            3            ;As in Mesa
 )

(tool-bar-mode 0)                       ;Disable toolbar
(menu-bar-mode 0)                       ;Disable menubar
(scroll-bar-mode 0)                     ;Disable scroll bar
(blink-cursor-mode 0)                   ;Stop blinking cursor
(fringe-mode '(8 . 0))                  ;Display fringe
(if (< emacs-major-version 26)
    (global-linum-mode 1)
  (global-display-line-numbers-mode t)) ;Show line numbers
(show-paren-mode 1)                     ;Highlight pair brackets
(global-visual-line-mode 1)             ;Wrap the line near the screen edge
(global-hl-line-mode 1)                 ;Highlight current line
(electric-pair-mode 1)                  ;Autocomplete paired characters
(fset 'yes-or-no-p 'y-or-n-p)           ;y/n instead of yes/no
(windmove-default-keybindings 'super)   ;Navigate windows with super
(set-default-coding-systems 'utf-8)     ;Default to UTF-8

;; Smoother scrolling
(when (display-graphic-p)
  (setq mouse-wheel-scroll-amount '(3 ((shift) . 3)) ;Three lines at a time
        mouse-wheel-progressive-speed nil            ;Don't accelerate
        mouse-wheel-follow-mouse t))                 ;Scroll under mouse
(setq scroll-step 1                                  ;Keyboard scrolling
      scroll-margin 0                                ;Scroll at the edge
      scroll-conservatively 101)                     ;No weird scrolling

;; Experimental
(setq sentence-end "\\([。！？]\\|……\\|[.?!][]\"')}]*\\($\\|[ \t]\\)\\)[ \t\n]*")

;; Change default backup directory
(setq user-backup-directory (concat user-emacs-directory "emacs-backups/"))
(unless (file-exists-p user-backup-directory)
  (make-directory user-backup-directory))
(setq backup-directory-alist `((".*" . ,user-backup-directory)))

;; Change default autosave directory
(setq user-autosave-directory (concat user-emacs-directory "emacs-autosave/"))
(unless (file-exists-p user-autosave-directory)
  (make-directory user-autosave-directory))
(setq auto-save-file-name-transforms `((".*" ,user-autosave-directory t)))

;; Change default custom-file
(setq custom-file (concat user-emacs-directory "custom.el"))
(when (file-exists-p custom-file)
  (load custom-file))

;; Because I'm running emacs as a server, starting it with a systemd
;; unit, it doesn't get a chance to read environment variables from my
;; .zshenv. Thus, I have to use this dirty hack.
(use-package exec-path-from-shell
  :ensure t)
(exec-path-from-shell-initialize)

;; Scroll a line without touching a mouse
(global-set-key (kbd "C-s-n") (lambda () (interactive) (scroll-up-line 1)))
(global-set-key (kbd "C-s-p") (lambda () (interactive) (scroll-down-line 1)))

;; Don't preserve font on copy
(add-to-list 'yank-excluded-properties 'font)
(add-to-list 'yank-excluded-properties 'face)

;; --------------------------------
;; Appearance
;; --------------------------------

;; Colour theme
(use-package ef-themes
  :ensure t
  :config
  (load-theme 'ef-autumn))

(use-package nerd-icons
  :ensure t)

;; Dashboard
(use-package dashboard
  :init
  (setq initial-buffer-choice (lambda () (get-buffer "*dashboard*"))
        dashboard-items '((recents  . 5)  ;Recent files
                          (projects . 5)) ;Open projects
        dashboard-set-heading-icons t     ;Heading icons
        dashboard-set-file-icons t        ;File icons
        dashboard-set-init-info t         ;Emacs started in ...
        dashboard-startup-banner 'logo    ;Nicer logo
        dashboard-set-navigator t         ;Buttons under the logo
        dashboard-navigator-buttons `(((,(nerd-icons-faicon "nf-fa-gear")
                                        "init.el"
                                        "Edit initialisation file"
                                        (lambda (&rest _)
                                          (open-user-init-file))))))
  :config
  (dashboard-setup-startup-hook))

;; Modeline

;; Change mode-line height
(set-face-attribute 'mode-line nil
                    :box '(:line-width 6 :color "#692a12"))

(set-face-attribute 'mode-line-inactive nil
                    :box '(:line-width 6 :color "#36322f"))

(defface sigexp/modeline-git-face
  '((t :inherit 'mode-line
       :background "#692a12"
       :foreground "#2fa526"))
  "Face for git info in the active modeline.")

(defun sigexp/propertize-icon (icon face_)
  "Propertize icon with face.

Just calling propertize on an icon removes :family attribute
which results in the icon being rendered incorrectly."
  (propertize icon 'face
              `(:inherit ,face_
                :family "Symbols Nerd Font Mono")))

(defvar-local sigexp/modeline-buffer-info
    '(:eval
      (list
       (let ((str (if buffer-read-only
                      (if (buffer-modified-p)
                          (nerd-icons-codicon "nf-cod-eye")
                        (nerd-icons-codicon "nf-cod-lock"))
                    (if (buffer-modified-p)
                        (nerd-icons-codicon "nf-cod-circle")
                      (nerd-icons-codicon "nf-cod-circle_filled")))))
         (propertize str 'face '(:family "Symbols Nerd Font Mono" :height 1.0)))
       " %I"))
  "Modeline format for buffer status and size.

Display different icons depending on buffer status:

* Unmodified: Filled circle
* Modified: Empty circle
* Read-only: Lock
* Read-only and modified: Eye

  Uses nerd-icons to insert an icon.")

(defvar-local sigexp/modeline-buffer-name
    '(:eval
      (list
       (nerd-icons-icon-for-mode major-mode)
       " "
       (buffer-name)))
  "Modeline format for buffer name.

Display buffer name and an icon associated with current
major-mode.

Uses nerd-icons to insert an icon.")

;; Unfortunately, this isn't properly update, unless something like "%l" is
;; present somewhere within mode-line-format
(defvar-local sigexp/modeline-selection-info
    '(:eval
      (when mark-active
        (propertize
         (concat
          (format "S:%d-" (line-number-at-pos (mark)))
          "%l ("                         ;Hence, this needs to be here
          (let* ((beg (region-beginning))
                 (end (region-end))
                 (lines (count-lines beg (min end (point-max))))
                 (chars (- end beg)))
            (format "%d L, %d C)" lines chars)))
         'face
         'mode-line-emphasis)))
  "Modeline format for selection (region) info.

Display the number of lines and characters in current active
region.")

(defvar-local sigexp/modeline-overwrite
    '(:eval
      (when overwrite-mode
        (nerd-icons-codicon "nf-cod-edit")))
  "Modeline format for overwrite-mode indicator.

Display a pencil icon if overwrite-mode is enabled.

Uses nerd-icons to insert an icon.")

(defvar-local sigexp/git-rev
    '(:eval
      (let* ((file (buffer-file-name))
             (backend (vc-backend file))
             (commit (substring (vc-working-revision file backend)
                                0 8))
             (branch (vc-git--symbolic-ref file))
             (state (vc-state file))
             (rev (or branch commit))
             (icon (if (equal state 'edited)
                       (nerd-icons-devicon "nf-dev-git_compare")
                       (if branch
                           (nerd-icons-devicon "nf-dev-git_branch")
                         (nerd-icons-devicon "nf-dev-git_commit")))))
        (if (mode-line-window-selected-p)
            (format "%s %s"
                    (sigexp/propertize-icon icon 'sigexp/modeline-git-face)
                    (propertize rev 'face 'sigexp/modeline-git-face))
          (format "%s %s" icon rev))))
  "Modeline format for git info.

Display current branch or commit along with an icon. Displays a
different icon if file contains uncommited changes.

Uses nerd-icons to insert an icon.")

(defvar-local sigexp/modeline-major-mode
    '(:eval
      (format "%s" (format-mode-line mode-name)))
  "Modeline format for major-mode info.")

(dolist (construct '(sigexp/modeline-buffer-info
                     sigexp/modeline-buffer-name
                     sigexp/modeline-selection-info
                     sigexp/modeline-overwrite
                     sigexp/git-rev
                     sigexp/modeline-major-mode))
  (put construct 'risky-local-variable t))

(setq-default mode-line-format
              '(" "
                sigexp/modeline-buffer-info
                "   "
                sigexp/modeline-buffer-name
                " "
                sigexp/modeline-overwrite
                " "
                sigexp/modeline-selection-info
                mode-line-format-right-align
                "   "
                sigexp/git-rev
                "   "
                sigexp/modeline-major-mode
                " "))

;; Highlight matching parens
(use-package paren
  :hook (after-init . show-paren-mode)
  :init (setq show-paren-when-point-inside-paren t
              show-paren-when-point-in-periphery t)
  :config
  (with-no-warnings
    ;; Display matching line for off-screen paren.
    (defun display-line-overlay (pos str &optional face)
      "Display line at POS as STR with FACE.

FACE defaults to inheriting from default and highlight."
      (let ((ol (save-excursion
                  (goto-char pos)
                  (make-overlay (line-beginning-position)
                                (line-end-position)))))
        (overlay-put ol 'display str)
        (overlay-put ol 'face
                     (or face '(:inherit highlight)))
        ol))

    (defvar-local show-paren--off-screen-overlay nil)
    (defun show-paren-off-screen (&rest _args)
      "Display matching line for off-screen paren."
      (when (overlayp show-paren--off-screen-overlay)
        (delete-overlay show-paren--off-screen-overlay))
      ;; Check if it's appropriate to show match info,
      (when (and (overlay-buffer show-paren--overlay)
                 (not (or cursor-in-echo-area
                          executing-kbd-macro
                          noninteractive
                          (minibufferp)
                          this-command))
                 (and (not (bobp))
                      (memq (char-syntax (char-before)) '(?\) ?\$)))
                 (= 1 (logand 1 (- (point)
                                   (save-excursion
                                     (forward-char -1)
                                     (skip-syntax-backward "/\\")
                                     (point))))))
        ;; Rebind `minibuffer-message' called by `blink-matching-open'
        ;; to handle the overlay display.
        (cl-letf (((symbol-function #'minibuffer-message)
                   (lambda (msg &rest args)
                     (let ((msg (apply #'format-message msg args)))
                       (setq show-paren--off-screen-overlay
                             (display-line-overlay
                              (window-start) msg 'popup-tip-face))))))
          (blink-matching-open))))
    (advice-add #'show-paren-function :after #'show-paren-off-screen)))

;; Fonts

(use-package fontaine
  :ensure t
  :hook
  ((after-init . fontaine-mode)
   (after-init . (lambda ()
                        ;; Set last preset or fall back to desired style from `fontaine-presets'.
                        (fontaine-set-preset (or (fontaine-restore-latest-preset) 'regular)))))
  :config
  (setq fontaine-presets
        '((regular)
          (t
           :default-family "Iosevka"
           :default-weight regular
           :default-height 140

           :fixed-pitch-family "Iosevka"
           :fixed-pitch-weight regular
           :fixed-pitch-height 1.0

           :variable-pitch-family "EB Garamond"
           :variable-pitch-height 180))))

(use-package ligature
  :ensure t
  :load-path "lisp/ligature"
  :config
  (ligature-set-ligatures
   'prog-mode
   ;; Iosevka ligatures for C
   '("->" ">=" "<=" "==" "!=" "++"))
  (global-ligature-mode 't))

;; --------------------------------
;; Handy stuff
;; --------------------------------

;; Copy filepath to the clipboard
(defun copy-filepath ()
 "Copy the current buffer file name to the clipboard"
 (interactive)
 (let ((filename (if (equal major-mode 'dired-mode)
                     default-directory
                   (buffer-file-name))))
   (when filename
     (kill-new filename)
     (message "Copied '%s" filename))))

(global-set-key (kbd "s-c f") 'copy-filepath)

;; Copy filepath and line number to the clipboard
(defun copy-filepath-and-linum ()
 "Copy the current buffer file name to the clipboard along with
 active line number"
 (interactive)
 (let ((filename-linum (concat (if (equal major-mode 'dired-mode)
                                   default-directory
                                 (buffer-file-name))
                               ":"
                               (number-to-string (line-number-at-pos)))))
   (when filename-linum
     (kill-new filename-linum)
     (message "Copied '%s" filename-linum))))

(global-set-key (kbd "s-c l") 'copy-filepath-and-linum)

;; Fill-sentence
(defun fill-sentence ()
  (interactive)
  ;; optional 
  ;; (save-excursion
  (backward-sentence)
  (push-mark)
  (forward-sentence)
  (fill-region (point) (mark))
  ;; )
  )

(global-set-key (kbd "s-q") 'fill-sentence)

;; Setup russian input method and immediately switch back to english
;; Input method then can be toggled with C-\
;; Compared to system-set keyboard layout, this has the advantage of emacs
;; keybings working properly regardless of current input method.
(set-input-method 'russian-computer)
(toggle-input-method)

;; Open init file
(defun open-user-init-file ()
  (interactive)
  (find-file user-init-file))

(global-set-key (kbd "s-x i") 'open-user-init-file)

;; --------------------------------
;; Interaction
;; --------------------------------

;; Dired
(use-package dired-open
  :after (dired)
  :bind
  (("<f8>"    . dired-other-window)  ;Start dired
   ("M-RET"   . dired-open-xdg)      ;Open with xdg-open
   :map dired-mode-map
   ("<prior>" . dired-up-directory)) ;Use PageUp to navigate up
  :custom
  ;; Options for ls
  (dired-listing-switches "-lhGA --group-directories-first")
  ;; Open with external app if not subdir
  (dired-open-functions '(dired-open-by-extension dired-open-subdir)))

(use-package nerd-icons-dired
  :hook
  (dired-mode . nerd-icons-dired-mode))

(use-package dired-git-info
  :bind (:map dired-mode-map
              (")" . dired-git-info-mode)))

;; Colourful dired
(use-package diredfl
  :init (diredfl-global-mode 1))

;; Show help after hitting prefix
(use-package which-key
  :config
  (setq which-key-idle-delay 0.5)       ;Reduce delay
  (which-key-mode))

;; Automatically reload files modified by external program
(use-package autorevert
  :hook (after-init . global-auto-revert-mode))

;; Better minibuffer
(use-package vertico
  :ensure t
  :config
  (vertico-mode 1))

;; Annotations within minibuffer
(use-package marginalia
  :ensure t
  :config
  (marginalia-mode 1))

;; Out-of-order patter matching within minibuffer
(use-package orderless
  :ensure t
  :config
  (setq completion-styles '(orderless basic)))

;; Better navigation within buffers, bookmarks, files, etc.
;; TODO: Figure out why live preview doesn't work for consult-line
(use-package consult
  :ensure t
  :bind
  (("M-s M-b" . consult-buffer)))

(use-package corfu
  :ensure t
  :config
  (global-corfu-mode 1)
  (setq corfu-auto t))

;; Better help screen
(use-package helpful
  :ensure t
  :bind
  (("C-h o" . helpful-symbol)
   ("C-h f" . helpful-function)
   ("C-h x" . helpful-command)
   ("C-h k" . helpful-key)
   ("C-h v" . helpful-variable)))

(use-package rg
  :bind
  (("C-c r r" . rg-project)))

;; Fancier window navigation
(use-package ace-window
  :init
  (progn
    ;; Number windows when "C-x o"
    (global-set-key [remap other-window] 'ace-window)
    ;; Enlarge font
    (custom-set-faces
     '(aw-leading-char-face
       ((t (:inherit ace-jump-head-foreground :height 2.0)))))))

;; Undo tree
(use-package undo-tree
  :init
  (global-undo-tree-mode)
  (setq undo-tree-visualizer-timestamps t)
  (with-no-warnings
    (make-variable-buffer-local 'undo-tree-visualizer-diff)
    (setq-default undo-tree-visualizer-diff t)))

;; Highlight lines that are too long and more
(use-package whitespace
  :config
  ;; (global-whitespace-mode 1)
  :custom
  (whitespace-style '(face empty tabs lines-tail trailing))
  (whitespace-line-column 78))

(use-package unicode-whitespace)

;; Expand selection to word, sentance, etc
(use-package expand-region
  :config
  :bind (("C-=" . er/expand-region)))

;; Iedit
;; Allows to edit multiple occurrences of a word
(use-package iedit)

;; Syntax checking
(use-package flycheck
  ;; :init
  ;; (global-flycheck-mode t)
  )

;; Spell checking
(use-package flyspell)

;; Use Perl Compatible Regular Expressions
(use-package pcre2el
  :config
  (pcre-mode))

;; Expand abbriviation to template
(use-package yasnippet
  :init
  (setq yas-prompt-functions '(yas-ido-prompt))
  (yas-global-mode 1))

;; A collection of snippets
(use-package yasnippet-snippets)

;; Fancier buffer list
(defalias 'list-buffers 'ibuffer-other-window)

;; With sections for different categories of buffers
(setq ibuffer-saved-filter-groups
      (quote (("default"
               ("Dired" (mode . dired-mode))
               ("org" (mode . org-mode))
               ("TeX" (name . "^.*tex$"))
               ("shell" (or (mode . eshell-mode)
                            (mode . shell-mode)
                            (mode . term-mode)))
               ("Code" (or (mode . c-mode)
                           (mode . haskell-mode)
                           (mode . lisp-mode)
                           (mode . racket-mode)
                           (mode . perl-mode)))
               ("Emacs" (or (mode . emacs-lisp-mode)))))))

(add-hook 'ibuffer-mode-hook
          (lambda ()
            (ibuffer-auto-mode 1)
            (ibuffer-switch-to-saved-filter-groups "default")))

;; Better buffer list
(use-package bufler
  :ensure t
  :bind
  (("C-x C-b" . bufler)))

;; Magit
;; Graphical git interface
(use-package magit
  :bind (("C-c m" . magit-status)))

(use-package git-messenger
  :config
  (setq git-messenger:use-magit-popup t)
  :bind
  (("C-c M-b" . git-messenger:popup-message)))

;; Rectangle selection
;; Press C-RET to start
(use-package cua-base
  :config (cua-selection-mode 1))

;; Needs to be checked
(use-package projectile
  :init
  (projectile-global-mode)
  (setq projectile-enable-caching t
        projectile-mode-line-prefix "Proj")
  :bind (("C-c M-f" . projectile-switch-project)
         ("C-c C-f" . projectile-find-file)
         ("C-c C-g" . projectile-grep)))

;; (use-package ggtags
;;   :ensure t
;;   :init
;;   :bind (("C-c C" . ggtags-create-tags)
;;          ("C-c U" . ggtags-update-tags)
;;          ("C-c D" . ggtags-delete-tags)
;;          ("C-c j" . ggtags-find-definition)
;;          ("C-c s" . ggtags-show-definition)
;;          ("C-c J" . ggtags-find-tag-dwim)
;;          ("C-c >" . ggtags-find-tag-continue)
;;          ("C-c C-f" . ggtags-find-file)))

(use-package diffview
  :init)

(use-package treemacs
  :config
  (setq treemacs-position 'right)
  :bind
  (("s-t" . treemacs)))

(use-package lsp-mode
  :commands lsp
  :config
  ;; lsp-lens-mode is awfully slow
  (setq lsp-lens-enable nil))

(use-package lsp-treemacs
  :after lsp-mode
  :bind (("s-l e" . lsp-treemacs-errors-list)
         ("s-l S" . lsp-treemacs-symbols)))

(use-package lsp-ui
  :commands lsp-ui-mode)

(use-package ccls
  :hook
  ((c-mode c++-mode objc-mode cuda-mode) .
   (lambda () (require 'ccls) (lsp))))

(use-package org-superstar
  :hook (org-mode . org-superstar-mode))

;; Looking up a word in a dictionary
(use-package lexic
  :bind
  (("M-s M-l RET" . lexic-search-word-at-point)
   ("M-s M-l /"   . lexic-search)))

(use-package shell-pop
  :custom
    (shell-pop-universal-key "H-t")
    (shell-pop-window-position "bottom")
    (shell-pop-term-shell "/bin/zsh"))

;; (use-package apostil)

;; Show minimap of the current buffer
(use-package minimap
  :init
  :bind
  (("s-c m" . minimap-mode))
  :config
  (setq minimap-window-location 'right))

;; Bookmarks
(use-package bm
  :demand t
  :init
  (setq bm-restore-repository-on-load t)
  :config
  (setq-default bm-buffer-persistence t)
  ;; Load bms on init
  (add-hook 'after-init-hook #'bm-repository-load)
  ;; Save bms
  (add-hook 'kill-buffer-hook #'bm-buffer-save)     ;On buffer kill
  (add-hook 'kill-emacs-hool #'(lambda nil          ;On emacs kill
                                 (bm-buffer-save-all)
                                 (bm-repository-save)))
  (add-hook 'after-save-hook #'bm-buffer-save)      ;On file save
  ;; Restore bms
  (add-hook 'find-file-hook #'bm-buffer-restore)    ;On file find
  (add-hook 'after-revert-hool #'bm-buffer-restore) ;On file revert
  :bind
  (("s-]"   . bm-next)
   ("s-["   . bm-previous)
   ("s-\\"  . bm-toggle)))

;; A pile of useful stuff
(use-package crux
  :ensure t
  :init
  :bind
  (("C-c o"   . crux-open-with)
   ("M-o"     . crux-smart-open-line-above)
   ("C-x 4 t" . crux-transpose-windows)
   ("C-c d"   . crux-duplicate-current-line-or-region)))

;; Focused reading/writing
(use-package olivetti
  :ensure
  :config
  (define-minor-mode sigexp/prosaic-mode
    "Enter olivetti-mode plus a couple more improvements.

 * Switch to proportional font
 * Disable line numbers
 * Disable current line highlighting
 * Change cursor to a blinking line
 * Hide modeline"
    :init-value nil
    :global nil
    (if sigexp/prosaic-mode
        (progn
          (olivetti-mode 1)
          (variable-pitch-mode 1)
          (display-line-numbers-mode -1)
          (hl-line-mode 0)
          (setq-local blink-cursor-interval 0.75
                      cursor-type '(bar . 2)
                      cursor-in-non-selected-windowsw 'hollow)
          (blink-cursor-mode 1)
          (setq-local mode-line-format nil))
      (olivetti-mode -1)
      (variable-pitch-mode -1)
      (display-line-numbers-mode 1)
      (hl-line-mode 1)
      (dolist (local '(blink-cursor-interval
                       cursor-type
                       cursor-in-non-selected-windows
                       mode-line-format))
        (kill-local-variable `,local))
      (blink-cursor-mode -1)))
  :bind ("C-c w" . sigexp/prosaic-mode))

;; --------------------------------
;; Modes
;; --------------------------------

(use-package haskell-mode)

(use-package pandoc-mode)

(use-package markdown-mode)

(use-package yaml-mode)

(use-package ninja-mode)

(use-package meson-mode)

(use-package glsl-mode
  :bind (("C-c h" . glsl-find-man-page)))

(use-package cmake-mode)

(use-package pkgbuild-mode)

(use-package bison-mode)

(use-package csv-mode)

(use-package rust-mode
  :hook (rust-mode . lsp))

;; (use-package systemd)

(use-package git-modes)

;; --------------------------------
;; Garbage
;; --------------------------------

;; C++ comment style
(defun c++-comment-style-hook-2 ()
  (set (make-local-variable 'comment-start) "/* ")
  (set (make-local-variable 'comment-end)   " */"))
(add-hook 'c++-mode-hook 'c++-comment-style-hook-2)
